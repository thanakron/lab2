package th.ac.tu.siit.calculator;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener 
{
    @Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        ((Button)findViewById(R.id.num0)).setOnClickListener(this);
        ((Button)findViewById(R.id.num1)).setOnClickListener(this);
        ((Button)findViewById(R.id.num2)).setOnClickListener(this);
        ((Button)findViewById(R.id.num3)).setOnClickListener(this);
        ((Button)findViewById(R.id.num4)).setOnClickListener(this);
        ((Button)findViewById(R.id.num5)).setOnClickListener(this);
        ((Button)findViewById(R.id.num6)).setOnClickListener(this);
        ((Button)findViewById(R.id.num7)).setOnClickListener(this);
        ((Button)findViewById(R.id.num8)).setOnClickListener(this);
        ((Button)findViewById(R.id.num9)).setOnClickListener(this);

        ((Button)findViewById(R.id.dot)).setOnClickListener(this);
        
        ((Button)findViewById(R.id.ac)).setOnClickListener(this);
        ((Button)findViewById(R.id.bs)).setOnClickListener(this);
        
        ((Button)findViewById(R.id.div)).setOnClickListener(this);
        ((Button)findViewById(R.id.mul)).setOnClickListener(this);
        ((Button)findViewById(R.id.sub)).setOnClickListener(this);
        ((Button)findViewById(R.id.add)).setOnClickListener(this);
        ((Button)findViewById(R.id.equ)).setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    private static String removeLastChar(String str) 
    {
        return str.substring(0,str.length()-1);
    }

    private double store;
    private boolean isCalculating;

    public Double getStore() 
    {
        return store;
    }

    public void setStore(String s) 
    {
        this.store = Double.parseDouble(s);
    }
    
    public boolean getState() 
    {
        return isCalculating;
    }
    
    public void setState(Boolean b) 
    {
        this.isCalculating = b;
    }
    
    public double calculate(double input1, double input2, String op)
    {
    	TextView output = (TextView)findViewById(R.id.output);
    	double result = 0;
    	if(op.equals("+"))
		{
			result = input1 + input2;
		}
		else if(op.equals("-"))
		{
			result = input1 - input2;
		}
		else if(op.equals("×"))
		{
			result = input1 * input2;
		}
		else if(op.equals("÷"))
		{
			if(input2 == 0)
			{
				output.setText("0");
			}
			else
			{
				result = input1 / input2;
			}
		}
    	return result;
    }
    
	@Override
	public void onClick(View v) 
	{
		int id = v.getId();
		
		TextView output = (TextView)findViewById(R.id.output);
		TextView operator = (TextView)findViewById(R.id.operator);
		String currentNumber = output.getText().toString();
		
		switch (id) 
		{
			case R.id.num0:
			case R.id.num1:
			case R.id.num2:
			case R.id.num3:
			case R.id.num4:
			case R.id.num5:
			case R.id.num6:
			case R.id.num7:
			case R.id.num8:
			case R.id.num9:
				if(currentNumber.equals("0"))
				{
					currentNumber = "";
				}
				currentNumber = currentNumber + ((Button)v).getText().toString();
				output.setText(currentNumber);
				break;
			
			case R.id.add:
			case R.id.mul:
			case R.id.div:
			case R.id.sub:
				if(getState())
				{
					String op = operator.getText().toString();
					double input1 = getStore();
					double input2 = Double.parseDouble(currentNumber);
					double result = calculate(input1, input2, op);
					setStore(result+"");
					setState(false);
				}
				// Keep the current number
				setStore(currentNumber);
				
				// Reset the input field
				currentNumber = "0";
				output.setText(currentNumber);
				
				// Set the operator field
				operator.setText(((Button)v).getText().toString());
				
				setState(true);
				break;
				
			case R.id.equ:
				double input1 = getStore();
				double input2 = Double.parseDouble(currentNumber);
				double result = 0;
				
				// Calculate
				String op = operator.getText().toString();
				
				result = calculate(input1, input2, op);
				
				// Display Result
				output.setText(result+"");
				// Store current number
				setStore(result+"");
				// Clear current operator 
				operator.setText("");

				break;
				
			case R.id.dot:
				String tmpCurrent = output.getText().toString();
				if(tmpCurrent.indexOf('.') == -1)
				{
					output.setText(tmpCurrent+".");
				}
				break;
				
			case R.id.ac:
				output.setText("0");
				operator.setText("");
				setStore("0");
				setState(false);
				break;
			
			case R.id.bs:
				currentNumber = removeLastChar(currentNumber);
				output.setText(currentNumber);
				break;
	
			default:
				break;
		}
	}
}
